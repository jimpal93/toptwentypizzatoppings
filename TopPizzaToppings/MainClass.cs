﻿using System;
using Newtonsoft.Json;
using System.Net;
using System.Collections.Generic;
using System.Linq;

namespace TopPizzaToppings
{
    class MainClass : PizzaType
    {
        public static string link = "https://www.brightway.com/CodeTests/pizzas.json";
        //The int is defined here so that there are no magic numbes in here
        //If there are alot of these constants we make a new class for them
        public const int TOP_TWENTY = 20;
        public const int CONFIG_ADDED_ONCE = 1;

        static void Main()
        {
            //To make sure there are no exceptions
            try
            {
                //Dictionary to store the type of pizza(or the pizza configuration) and the amount of times its ordered
                Dictionary<string, int> PizzaTypeAndNum = new Dictionary<string, int>();

                if (GetSerializedJSONString() != null)
                {
                    //The list that stores the derialized json data
                    List<PizzaType> pizzaTypes = JsonConvert.DeserializeObject<List<PizzaType>>(GetSerializedJSONString());

                    for (int i = 0; i < pizzaTypes.Count; i++)
                    {
                        string configuration = string.Join(",", pizzaTypes[i].Toppings.OrderBy(x => x));

                        if (PizzaTypeAndNum.ContainsKey(configuration))
                        {
                            PizzaTypeAndNum[configuration]++;
                        }
                        else
                        {
                            PizzaTypeAndNum.Add(configuration, CONFIG_ADDED_ONCE);
                        }
                    }

                    Console.WriteLine("Top 20 Pizza configs \n" + string.Join("\n", PizzaTypeAndNum.OrderByDescending(x => x.Value).Take(TOP_TWENTY)
                        .Select(x => "configuration - " + x.Key + " ordered - " + x.Value.ToString() + " times")));

                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        /// <summary>
        /// This method gets the serialized version of the json in a string format to be deserialized later
        /// </summary>
        /// <returns>string</returns>
        private static string GetSerializedJSONString()
        {

            WebClient webClient = new WebClient();

            try
            {
                string serializedString = webClient.DownloadString(link);
                return serializedString;
            }
            catch (WebException we)
            {

                Console.WriteLine(we.Message);
                return null;
            }


        }

    }
}
