﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TopPizzaToppings
{
    /// <summary>
    /// This is the class that defines they type of pizza it is based on toppings
    /// </summary>
    class PizzaType
    {
        /// <summary>
        /// This list is the list of toppings that will deserialized from the JSON string.
        /// </summary>
        public List<string> Toppings
        {
            get;
            set;
        }


    }
}
